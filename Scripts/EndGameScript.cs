﻿/* Memory Game
 * EndGameScript.cs Script
 * Robert-Jan Keizer */

// Used Libraries
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Main class that handles the end of a game
public class EndGameScript : MonoBehaviour {
	
	// If last game won print "You won". If last game lost print "You lost".
	void Start () {
		if (GetBool("lastGameWon"))
			GameObject.Find("MemoryGame").GetComponent<TextMeshProUGUI>().text = "You won the game!";
		else {
			GameObject.Find("MemoryGame").GetComponent<TextMeshProUGUI>().text = "You lost it.";
		}

		GameObject.Find("HighScore").GetComponent<TextMeshProUGUI>().text = PlayerPrefs.GetInt("lastGameScore").ToString();
	}
	
	// gets last game is won or not from PlayerPrefs
	public static bool GetBool (string name)
	{
		return PlayerPrefs.GetInt(name) == 1;
	}
}
