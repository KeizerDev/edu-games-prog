﻿/* Memory Game
 * SettingsEvents.cs Script
 * Robert-Jan Keizer */

// Used Libraries
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// Settings handler
public class SettingsEvents : MonoBehaviour {

	// Total words
	private string[] words;
	// Active game words
	private string[] gameWords;

	// Total words as a list
	List<string> wordsAsAList;
	// Active game words as a list
	List<string> gameWordsAsAList;
	// Total words dropdown object
	private Dropdown total_Dropdown;
	// Active game words dropdown object
	private Dropdown game_Dropdown;

	void Start () {

		// Gets player name
		string name = PlayerPrefs.GetString("name");
		// If it is initiated before, assign it to the Placeholder of the text input (name)
		if (name != null)
			GameObject.Find("Name").GetComponent<InputField>().placeholder.GetComponent<Text>().text  = name;
	}

	// Save the user's input (name) to the PlayerPrefs (DB)
	public void saveName(){
		string name = GameObject.Find("Name").GetComponent<InputField>().text;
		PlayerPrefs.SetString("name", name);
	}

	// Resets highscores
	public void resetHighScores(){
		// Creates new arrays
		string[] highNames = new string[5];
		int[] highScores = new int[5];

		// Fill them with default data
		for (int i = 0; i < highScores.Length; i++)
		{
			highScores[i] = 0;
			highNames[i] = "noname";
		}

		// Update them in the PlayerPrefs (DB)
		PlayerPrefsNew.SetIntArray("highscores", highScores);
		PlayerPrefsNew.SetStringArray("highnames", highNames);
	}
}
