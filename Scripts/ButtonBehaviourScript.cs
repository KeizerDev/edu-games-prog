﻿/* Memory Game
 * ButtonBehaviourScript.cs Script
 * Robert-Jan Keizer */

// Used Libraries
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

// Main Class that handles navigation
public class ButtonBehaviourScript : MonoBehaviour {

	// load scene, navigation to Settings
	public void settings(){
		SceneManager.LoadScene ("Settings");
	}
	// load scene, navigation to Play (Level)
	public void play(){
		SceneManager.LoadScene ("Level");
	}

	// load scene, navigation to MainMenu
	public void toMain(){
		SceneManager.LoadScene ("MainMenu");
	}

	// load scene, navigation to Word Settings
	public void toWordSettings(){
		SceneManager.LoadScene ("WordSettings");
	}
}
