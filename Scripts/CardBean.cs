﻿/* Memory Game
 * CardBean.cs Script
 * Robert-Jan Keizer */

// Used Libraries
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;


// Main class that handles cards
[Serializable]
public class CardBean : MonoBehaviour {

    public bool lang;
    public string text;
    public bool opened = false;

	// Card initialization
	void Start () {
        TextMeshPro tmp = gameObject.transform.GetChild(0).transform.GetComponentInChildren<TextMeshPro>(false);
        tmp.text = getText();
	}

    // getter & setters
    void setText(string text)
    {
        this.text = text;
    }

    void setLang(bool lang)
    {
        this.lang = lang;
    }

    bool getLang()
    {
        return this.lang;
    }

    string getText()
    {
        return this.text;
    }

    // When clicked on card
    void OnMouseDown()
    {
        GameObject.Find("_levelSystem").GetComponent<levelManager>().pressed(this.gameObject);
        
    }

    // open animation and opened true
    public void open(){
        Animator animator = gameObject.transform.GetComponentInChildren<Animator>();
        animator.SetTrigger("opener");
        opened = true;
    }

    // close animation and opened false
    public void close(){
        Animator animator = gameObject.transform.GetComponentInChildren<Animator>();
        animator.SetTrigger("closer");
        opened = false;
    }

}

