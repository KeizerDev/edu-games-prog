﻿/* Memory Game
 * MainMenuScript.cs Script
 * Robert-Jan Keizer*/

// Used Libraries
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

// Main class that handles menu attributes
public class MainMenuScript : MonoBehaviour {

	// Highscore values
	private int[] highScores;
	// High score values
    private string[] highNames;
	// The text to be printed
	private string stats = "";
	void Start () {

		// In the first start "name" will be null or empty so it is assigning "unnamed" to it, so no worries about the name
		string name = PlayerPrefs.GetString("name");
		if (name.Length < 1){
			PlayerPrefs.SetString("name", "unnamed");
		}
		
		// Get highscores from PlayerPrefs
		highScores = PlayerPrefsNew.GetIntArray("highscores");
        highNames = PlayerPrefsNew.GetStringArray("highnames");

		// If not initiated, initiate with noname...0
		if (highNames.Length <= 0){
			highNames = new string[5];
			highScores = new int[5];

			for (int i = 0; i < highScores.Length; i++)
			{
				highScores[i] = 0;
				highNames[i] = "noname";
			}
			// Update new values on PlayerPrefs
			PlayerPrefsNew.SetIntArray("highscores", highScores);
			PlayerPrefsNew.SetStringArray("highnames", highNames);
		}
		
		// Create string from values
		for (int i = 0; i < highScores.Length; i++)
		{	
			stats += highNames[i] + "..." + highScores[i] + "\n" ;
		}

		// Change Text on the UI with the generated text
		GameObject.Find("HighScore").GetComponent<TextMeshProUGUI>().text = stats;
		
		
	}
	
}
