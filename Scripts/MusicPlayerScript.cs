﻿/* Memory Game
 * MusicPlayerScript.cs Script
 * RObert-Jan Keizer */

// Used Libraries
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Class used to start background Music
public class MusicPlayerScript : MonoBehaviour {
    
    void Start()
    {

    }

    private static MusicPlayerScript instance = null;
    public static MusicPlayerScript Instance
    {
        get { return instance; }
    }
    
    // Checks if music is playing. If so keep it going, if not start music
    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        
        DontDestroyOnLoad(this.gameObject);
    }
    void Update()
    {

    }

}
