﻿/* Memory Game
 * WordSettingsEvents.cs Script
 * Robert-Jan Keizer */

// Used Libraries
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


// Settings handler
public class WordSettingsEvents : MonoBehaviour {

	// Total words
	private string[] words;
	// Active game words
	private string[] gameWords;

	// Total words as a list
	List<string> wordsAsAList;
	// Active game words as a list
	List<string> gameWordsAsAList;
	// Total words dropdown object
	private Dropdown total_Dropdown;
	// Active game words dropdown object
	private Dropdown game_Dropdown;

	string output;

	void Start () {

		initializeDropdowns();
		initializeExportAndImport();
	}

    // Void used to get data ready for import/export
	private void initializeExportAndImport(){

		output = "";

		for (int i = 0; i < words.Length; i++)
		{
			output += words[i];
			output += "\n";
		}

		Debug.Log(output);

		if (output.Length > 1){
			GameObject.Find("Long").GetComponent<InputField>().text = output;
		}
	}

    // Void used to export all words in a deck
	public void export(){

		output = "";

		for (int i = 0; i < words.Length; i++)
		{
			output += words[i];
			output += "\n";
		}

		Debug.Log(output);

		if (output.Length > 1){
			GameObject.Find("Long").GetComponent<InputField>().text = output;
		}

		TextEditor te = new TextEditor();
		te.text = output;
		te.SelectAll();
		te.Copy();

		GameObject.Find("Clipboard").GetComponent<Text>().text = "Copied to clipboard!";
		StartCoroutine(deleteTheThext());
	}

    // Void used to import from textbox
	public void importFromTextBox(){
		string newWords = GameObject.Find("Long").GetComponent<InputField>().text;
		importWords(newWords);
		GameObject.Find("Clipboard").GetComponent<Text>().text = "Imported!";
		StartCoroutine(deleteTheThext());
	}

    // Void used to import from clipboard
	public void importFromClipboard(){
		string newWords = GUIUtility.systemCopyBuffer;
		Debug.Log(newWords);
		importWords(newWords);
		GameObject.Find("Clipboard").GetComponent<Text>().text = "Imported from clipboard!";
		StartCoroutine(deleteTheThext());
	}

    // Void used to add imported words to the game
	private void importWords(string importedWords){

		string[] arr =  importedWords.Split('\n');

		for (int j = 0; j < arr.Length; j++)
		{
			string word = arr[j];

			// If it can parse it with "," , it means there are 2 words
			try
			{
				word.Split(',');
			}
			// Else pass that line
			catch (System.Exception)
			{
				continue;
			}

			if(!wordsAsAList.Contains(word) && word.Length > 1){

				// Moves instances into new array
				string[] newWords = new string[words.Length + 1];
				for (int i = 0; i < words.Length; i++)
				{
					newWords[i] = words[i];
				}

                // Moves instances into new array
                newWords[words.Length] = word;
				words = newWords;
				wordsAsAList = new List<string>(words);
				wordsAsAList.Reverse();
				PlayerPrefsNew.SetStringArray("words", words);
				total_Dropdown.ClearOptions();
				total_Dropdown.AddOptions(wordsAsAList);
			}
		}
	}

	 IEnumerator deleteTheThext()
    {
        yield return new WaitForSeconds(1f);
        GameObject.Find("Clipboard").GetComponent<Text>().text = "";
    }
	

	private void initializeDropdowns(){
		// Gets words from PlayerPrefs
		words = PlayerPrefsNew.GetStringArray("words");
		// Gssigns total dropdown from UI
		total_Dropdown = GameObject.Find("AllDropdown").GetComponent<Dropdown>();
		
		// Generates list from array to use it in dropdown methods
		wordsAsAList = new List<string>(words);
		// Clear dropdowns options
		total_Dropdown.ClearOptions();
		// reverse the list (last added will be first)
		wordsAsAList.Reverse();
		// Add list to the dropdown options
		total_Dropdown.AddOptions(wordsAsAList);

		/*
			Same procedure for the active game words
		 */
		gameWords = PlayerPrefsNew.GetStringArray("gameWords");
		game_Dropdown = GameObject.Find("GameDropdown").GetComponent<Dropdown>();

		gameWordsAsAList = new List<string>(gameWords);
		game_Dropdown.ClearOptions();
		gameWordsAsAList.Reverse();
		game_Dropdown.AddOptions(gameWordsAsAList);
	}

	// Saves the user's input (name) to the PlayerPrefs (DB)
	public void saveName(){
		string name = GameObject.Find("Name").GetComponent<InputField>().text;
		PlayerPrefs.SetString("name", name);
	}

	// Adds new word pair to the game (total list)
	public void addNewWordPair(){
		string english = GameObject.Find("English").GetComponent<InputField>().text;
		string spanish = GameObject.Find("Spanish").GetComponent<InputField>().text;
		string word = english + "," + spanish;

		// If both are not empty and this pair is not added before
		if (english != "" && spanish != "" && !wordsAsAList.Contains(word)){
			GameObject.Find("English").GetComponent<InputField>().text = "";
			GameObject.Find("Spanish").GetComponent<InputField>().text = "";
			
			// Moves instances into new array
			string[] newWords = new string[words.Length + 1];
			for (int i = 0; i < words.Length; i++)
			{
				newWords[i] = words[i];
			}

            // Moves instances into new array
            newWords[words.Length] = word;
			words = newWords;
			wordsAsAList = new List<string>(words);
			wordsAsAList.Reverse();
			PlayerPrefsNew.SetStringArray("words", words);
			total_Dropdown.ClearOptions();
			total_Dropdown.AddOptions(wordsAsAList);
		}
	}

	// Gets the word pair from total list and adds to the active game
	public void addToGameWords(){
		// get the word pair
		string wordsPair = wordsAsAList[total_Dropdown.value];
		// If it wont overload the game (<13) and haven't added before
		if (gameWordsAsAList.Count < 13 && !gameWordsAsAList.Contains(wordsPair)){
			string[] newGameWords = new string[gameWords.Length + 1];

			// Moves instances to new array
			for (int i = 0; i < gameWords.Length; i++)
			{
				newGameWords[i] = gameWords[i];
			}

            // Moves instances to new array
            newGameWords[gameWords.Length] = wordsPair;
			gameWords = newGameWords;
			gameWordsAsAList = new List<string>(gameWords);
			gameWordsAsAList.Reverse();
			PlayerPrefsNew.SetStringArray("gameWords", gameWords);
			game_Dropdown.ClearOptions();
			game_Dropdown.AddOptions(gameWordsAsAList);
		}
	}

	// Removes the selected word from total words
	public void removeFromTotal(){
		// Get the selected pair
		string wordsPair = wordsAsAList[total_Dropdown.value];
		string[] newWords = new string[words.Length - 1];
		
		// deletes the set
		for (int i = 0, j = 0; i < words.Length; i++, j++)
		{
			if (wordsPair.Equals(words[i])){
				j--;
			} else {
				newWords[j] = words[i];
			}
		}

        // Moves instances to new array
        words = newWords;
		wordsAsAList = new List<string>(words);
		wordsAsAList.Reverse();
		PlayerPrefsNew.SetStringArray("words", words);
		total_Dropdown.ClearOptions();
		total_Dropdown.AddOptions(wordsAsAList);
	}

    // Removes the selected word from total words
    public void removeFromGame(){
		// if it won't delete the last 2 elements
		if (gameWordsAsAList.Count > 2){
			string wordsPair = gameWordsAsAList[game_Dropdown.value];
			string[] newWords = new string[gameWords.Length - 1];
			for (int i = 0, j = 0; i < gameWords.Length; i++, j++)
			{
				if (wordsPair.Equals(gameWords[i])){
					j--;
				} else {
					newWords[j] = gameWords[i];
				}
			}

            // Moves instances to new array
            gameWords = newWords;
			gameWordsAsAList = new List<string>(gameWords);
			gameWordsAsAList.Reverse();
			PlayerPrefsNew.SetStringArray("gameWords", gameWords);
			game_Dropdown.ClearOptions();
			game_Dropdown.AddOptions(gameWordsAsAList);
		}
		
	}

	// Resets highscores
	public void resetHighScores(){
		// Creates new arrays
		string[] highNames = new string[5];
		int[] highScores = new int[5];

		// Fills them with default data
		for (int i = 0; i < highScores.Length; i++)
		{
			highScores[i] = 0;
			highNames[i] = "noname";
		}

		// Updates them in the PlayerPrefs (DB)
		PlayerPrefsNew.SetIntArray("highscores", highScores);
		PlayerPrefsNew.SetStringArray("highnames", highNames);
	}
}
