﻿/* Memory Game
 * LevelManger.cs Script
 * Robert-Jan Keizer */

// Used Libraries
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using System.Timers;
using System.Diagnostics;

// Main class that handles the level
public class levelManager : MonoBehaviour
{
    // Variables used to determine card and card position
    public GameObject card;
    public int cardNumber;
    public float initialX = -6.87f;
    public float initialY = 1.69f;
    public float initialZ = 9.1f;
    public float gap = 1.3f;

    public List<string> eng = new List<string>();
    public List<string> spa = new List<string>();

    string[] words;
    private List<string> list = new List<string>();

    GameObject first;
    GameObject second;
    int wordsLeft;

    // Variable used for highscores
    int[] highScores = new int[5];
    string[] highNames = new string[5];

    enum ArrayType { Float, Int32, Bool, String, Vector2, Vector3, Quaternion, Color }
    static private int endianDiff1;
    static private int endianDiff2;
    static private int idx;
    static private byte[] byteBlock;

    private int score = 0;

    private Stopwatch _countdownTime;
    private const int _timeRemaining = 180;

    public TextMeshPro timerText;

    public TextMeshPro cardsText;


    // Play mode initializations
    void Start()
    {
        // Last game score reset
        PlayerPrefs.SetInt("lastGameScore", 0);
        PlayerPrefsNew.SetBool("lastGameWon", true);

        // Get words from PlayerPrefs
        getWordsFromPlayerPrefs();
        // Set words to the necessary instances
        setLists();

        // Word counter
        wordsLeft = words.Length;

        // Total cards number
        cardNumber = list.Count;

        // Generating card objects and instantiate them on necessary coordinates
        for (int i = 0; i < cardNumber; i++)
        {
            if (i == 9 || i == 18)
            {

                initialY -= gap * 2;
                var obj = Instantiate(card, new Vector3(initialX + gap * (i % 9), initialY, initialZ), Quaternion.identity);

                int randomIndex = UnityEngine.Random.Range(0, list.Count);
                obj.AddComponent<CardBean>().text = list[randomIndex];
                if (eng.Contains(list[randomIndex]))
                {
                    obj.GetComponent<CardBean>().lang = true;
                }
                else
                {
                    obj.GetComponent<CardBean>().lang = false;
                }
                list.RemoveAt(randomIndex);
            }
            else
            {
                var obj = Instantiate(card, new Vector3(initialX + gap * (i % 9), initialY, initialZ), Quaternion.identity);
                int randomIndex = UnityEngine.Random.Range(0, list.Count);
                obj.AddComponent<CardBean>().text = list[randomIndex];
                if (eng.Contains(list[randomIndex]))
                {
                    obj.GetComponent<CardBean>().lang = true;
                }
                else
                {
                    obj.GetComponent<CardBean>().lang = false;
                }
                list.RemoveAt(randomIndex);
            }
        }

        // Starts time counter
        _countdownTime = new Stopwatch();
        _countdownTime.Start();
    }
    
    // Runs before every frame rendered
    void Update()
    {
        // Updates the time remaining and cards left
        if (_countdownTime.IsRunning)
        {
            GameObject.Find("Timing").GetComponent<TextMeshProUGUI>().text = "Time Remaining: " + (int)(_timeRemaining - _countdownTime.Elapsed.TotalSeconds);
            GameObject.Find("Cards").GetComponent<TextMeshProUGUI>().text = "Words Remaining: " + wordsLeft + " Words";
        }

        // If time is up, game over
        if (_countdownTime.Elapsed.TotalSeconds > _timeRemaining)
        {
            StartCoroutine("end");
        }

    }

    // Set highscores to the local DB for unity, PlayerPrefs
    void setHighScores()
    {
        highScores = PlayerPrefsNew.GetIntArray("highscores");
        highNames = PlayerPrefsNew.GetStringArray("highnames");
        string tempString = "", tempString2;
        int tempInt = 0, tempInt2;
        int i = 0;
        for (; i < highScores.Length; i++)
        {
            if (score > highScores[i])
            {
                tempInt = highScores[i];
                tempString = highNames[i];
                highScores[i] = score;
                highNames[i] = PlayerPrefs.GetString("name");
                break;
            }
        }
        i++;

        for(; i < highScores.Length; i++){
                tempString2 = highNames[i];
                tempInt2 = highScores[i];
                highNames[i] = tempString;
                highScores[i] = tempInt;
                tempString = tempString2;
                tempInt = tempInt2;
        }

        PlayerPrefsNew.SetIntArray("highscores", highScores);
        PlayerPrefsNew.SetStringArray("highnames", highNames);
    }

    // When pressed to the card
    public void pressed(GameObject card)
    {
        // Logging cards
        UnityEngine.Debug.Log(card.transform.GetChild(0).transform.GetComponentInChildren<TextMeshPro>(false).text);

        // If first card isn't clicked
        if (!first)
        {
            first = card;
            CardBean firstCardClass = first.GetComponent<CardBean>();

            // Open the card if its not opened
            if (!firstCardClass.opened)
            {
                firstCardClass.open();
            }
        }
        // If first card is opened and second isn't opened
        else if (!second)
        {
            second = card;

            CardBean secondCardClass = second.GetComponent<CardBean>();

            if (!secondCardClass.opened)
            {
                secondCardClass.open();
            }

            /*
                From line 203 to 218 is about checking if turned cards are a match.
             */
            TextMeshPro firstTmp = first.transform.GetChild(0).transform.GetComponentInChildren<TextMeshPro>(false);
            TextMeshPro secondTmp = second.transform.GetChild(0).transform.GetComponentInChildren<TextMeshPro>(false);
            bool engFirst = first.GetComponent<CardBean>().lang;
            int indexOfFirst;
            int indexOfSecond;
            if (engFirst)
            {
                indexOfFirst = eng.IndexOf(firstTmp.text);
                indexOfSecond = spa.IndexOf(secondTmp.text);
            }
            else
            {
                indexOfFirst = spa.IndexOf(firstTmp.text);
                indexOfSecond = eng.IndexOf(secondTmp.text);
            }

            // If they are the same card pairs
            if (indexOfFirst == indexOfSecond)
            {
                UnityEngine.Debug.Log("true");
                // Decrease the words left
                wordsLeft--;

                // If no cards left
                if (wordsLeft <= 0)
                {   
                    // End game as winner
                    StartCoroutine("won");
                }

                // If there are still cards, reset first and second
                first.GetComponent<BoxCollider2D>().enabled = false;
                second.GetComponent<BoxCollider2D>().enabled = false;
                StartCoroutine(reseter());
            }
            // If they are not same pair
            else
            {
                UnityEngine.Debug.Log("false");
                // Close cards
                StartCoroutine(closer(first, second));
            }

        }
    }

    // Closer of the cards after 1 second, and calls reset
    IEnumerator closer(GameObject first, GameObject second)
    {
        yield return new WaitForSeconds(1);
        first.GetComponent<CardBean>().close();
        second.GetComponent<CardBean>().close();
        StartCoroutine(reseter());
    }

    // Reset the clicked pairs; first, second
    IEnumerator reseter()
    {
        yield return new WaitForSeconds(0.5f);
        first = null;
        second = null;
    }

    // End the game as loser
    IEnumerator end()
    {
        yield return new WaitForSeconds(1f);
        // Update lastGame data on PlayerPrefs
        PlayerPrefs.SetInt("lastGameScore", 0);
        PlayerPrefsNew.SetBool("lastGameWon", false);
        // Load endGame Screen.
        SceneManager.LoadScene("EndGame");
    }

    // End the game as winner
    IEnumerator won(){
        yield return new WaitForSeconds(1f);
        // Calculate score
        score = calculateScore();
        // Update lastGame data on PlayerPrefs
        PlayerPrefs.SetInt("lastGameScore", score);
        PlayerPrefsNew.SetBool("lastGameWon", true);
        // Set highscore data on PlayerPrefs
        setHighScores();
        // Load endGame Screen
        SceneManager.LoadScene("EndGame");
    }

    // Score calculator
    private int calculateScore()
    {
        int successful = (cardNumber/2 - wordsLeft) * 10;
        int temp = (int)(_timeRemaining - _countdownTime.Elapsed.TotalSeconds);
        return temp + successful;
    }

    // Gets words from playerprefs
    private void getWordsFromPlayerPrefs()
    {
        this.words = PlayerPrefsNew.GetStringArray("gameWords");
    }

    // Sets words to necessary instances of lists
    private void setLists()
    {
        foreach (string wordPair in this.words)
        {
            string[] parsedString = wordPair.Split(',');
            this.eng.Add(parsedString[0]);
            this.spa.Add(parsedString[1]);
            this.list.Add(parsedString[0]);
            this.list.Add(parsedString[1]);
        }
    }

}
